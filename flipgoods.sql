/*
SQLyog - Free MySQL GUI v5.01
Host - 5.5.20 : Database - flipgoods
*********************************************************************
Server version : 5.5.20
*/


create database if not exists `flipgoods`;

USE `flipgoods`;

SET FOREIGN_KEY_CHECKS=0;

/*Table structure for table `category` */

DROP TABLE IF EXISTS `category`;

CREATE TABLE `category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `categoryName` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `imageUrl` varchar(255) DEFAULT NULL,
  `optLockVersion` int(11) NOT NULL,
  `totalProducts` bigint(20) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  `ebayCategoryId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `category` */

insert into `category` values 
(1,'Book','All about books','',1,9,'',NULL);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `firstName` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  `optLockVersion` int(11) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone` bigint(20) NOT NULL,
  `userName` varchar(50) NOT NULL,
  `userStatus` varchar(255) DEFAULT NULL,
  `userRole_id` bigint(20) DEFAULT NULL,
  `activationCode` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_e6gkqunxajvyxl5uctpl2vl2p` (`email`),
  KEY `FK_p98fkki857lcnwgie2dyedmc2` (`userRole_id`),
  CONSTRAINT `FK_p98fkki857lcnwgie2dyedmc2` FOREIGN KEY (`userRole_id`) REFERENCES `userrole` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `user` */

insert into `user` values 
(1,'flipadmin@gmail.com','flip','admin',1,'$2a$10$g7U7dmiLG6LcFaAlXXkQiO0atK3.T9dvM905oMWRbWl.V97ZPu3MW',9876545678,'flipadmin','ACTIVE',3,'NULL');

/*Table structure for table `userrole` */

DROP TABLE IF EXISTS `userrole`;

CREATE TABLE `userrole` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `optLockVersion` int(11) NOT NULL,
  `userRole` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `userrole` */

insert into `userrole` values 
(1,1,'RETAILER'),
(2,1,'SUPPLIER'),
(3,1,'ADMIN');

SET FOREIGN_KEY_CHECKS=1;
