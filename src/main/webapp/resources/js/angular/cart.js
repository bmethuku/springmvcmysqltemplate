app.controller("CartController", function($scope, $http){
	
	$scope.orderItemsBySuppliers = [];
	$scope.total = 0;
	
	$scope.setTotal = function(){	
		var sum = 0;
		var orderItemsBySuppliers = $scope.orderItemsBySuppliers;		
		$.each( orderItemsBySuppliers, function( index, orderItemsBySupplier ){			
			$.each(orderItemsBySupplier.orderItems ,function( index, orderItem ){
				sum += orderItem.product.cost * orderItem.quantity;
			});			
		});
		$scope.total = sum;
	}
		
	$scope.addToCart = function(productIds, callback) {
		$http({
				url: contextPath + 'retailer/cart/addItems',
				method: 'POST',
				dataType : 'json',		
				data: "productIds="+ productIds.join(),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		
			}).then(function(dataResponse) {
	        	var response = dataResponse.data;
	        	$scope.parseResponse(response);        	
	        	$(".cart-modal").modal("show");
	        	
	        	// Make sure the callback is a function
	            if (typeof callback === "function") {	            
	                callback();
	            }	            	            	          
	        	$scope.setTotal();
	    	});		
	};	
	
	$scope.parseResponse = function(response){	
		 		 
		 $scope.orderItemsBySuppliers =[];		
         var cartBySupplier =  response.cartMapBySupplier;         
         for ( key in cartBySupplier ) {
         	var obj = new Object();
         	obj.name = key;
         	obj.orderItems = cartBySupplier[key];
         	$scope.orderItemsBySuppliers.push(obj);	            	
         }		
	} 
	
	$scope.deleteOrderItem = function(){
		var orderItem = this.orderItem;	
		$http({
			url: contextPath + 'retailer/cart/deleteOrderItem/' + orderItem.id ,
			method: 'GET',
			dataType : 'json',
		}).then(function(dataResponse) {
        	var response = dataResponse.data;        	
        	$scope.parseResponse(response);
        	$scope.setTotal();
    	});
		return;
	};
	
	$scope.updateOrderItem = function(){
		var orderItem = this.orderItem;		
		try { 
			var q = orderItem.quantity;
			
			if(orderItem.quantity == ""){
				return;
			}
			
			if (!(isNormalInteger(q))){
				orderItem.quantity = 1;
			}			
		} catch(e){
			orderItem.quantity = 1;
		}
				
		if(orderItem.quantity <= 0){
			orderItem.quantity = 1;
		}
		
		var formdata = "quantity=" + orderItem.quantity + "&cartId=" + orderItem.cartId + 
			"&id=" + orderItem.id; 
		$http({
			url: contextPath + 'retailer/cart/updateOrderItem',
			method: 'POST',
			dataType : 'json',		
			data: formdata,
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		}).then(function(dataResponse) {
        	var response = dataResponse.data; 
        	$scope.setTotal();
    	});
		return;
	};
});