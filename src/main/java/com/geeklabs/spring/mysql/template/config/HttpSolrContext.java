package com.geeklabs.spring.mysql.template.config;
/*package com.flipgoods.liquidation.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
//import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.data.solr.repository.config.EnableSolrRepositories;
import org.springframework.data.solr.server.support.HttpSolrServerFactoryBean;

//import com.flipgoods.liquidation.solr.repository.ProductDocRepository;
//import com.flipgoods.liquidation.solr.repository.impl.ProductDocRepositoryImpl;

@Configuration
@EnableSolrRepositories("com.flipgoods.liquidation.solr.repository")
@Profile("prod")
@PropertySource("classpath:application.properties")
public class HttpSolrContext {

	
    private static final String PROPERTY_NAME_SOLR_SERVER_URL = "solr.server.url";

    @Autowired
    private Environment environment;

    @Bean
    public HttpSolrServerFactoryBean solrServerFactoryBean() {
        HttpSolrServerFactoryBean factory = new HttpSolrServerFactoryBean();

        factory.setUrl(environment.getRequiredProperty(PROPERTY_NAME_SOLR_SERVER_URL));

        return factory;
    }

    @Bean
    public SolrTemplate solrTemplate() throws Exception {
        return new SolrTemplate(solrServerFactoryBean().getObject());
    }
    
    @Bean
    public ProductDocRepository productDocRepository() throws Exception {
	    return (ProductDocRepository) new ProductDocRepositoryImpl(solrTemplate());
    }
}*/