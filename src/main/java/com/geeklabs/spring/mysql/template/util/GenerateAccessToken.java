package com.geeklabs.spring.mysql.template.util;

import java.util.Map;

import com.paypal.core.rest.OAuthTokenCredential;
import com.paypal.core.rest.PayPalRESTException;

public class GenerateAccessToken {

	public static String getAccessToken(String clientID, String clientSecret, Map<String, String> sdkConfig) {
		String accessToken = null;
		try {
			accessToken =  new OAuthTokenCredential(clientID, clientSecret, sdkConfig).getAccessToken();
		} catch (PayPalRESTException e) {
			e.printStackTrace();
		}
		return accessToken;
	}

}
