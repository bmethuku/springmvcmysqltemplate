package com.geeklabs.spring.mysql.template.domain.repository.spec;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.geeklabs.spring.mysql.template.domain.User;

public class UserSpecification {

	public static Specification<User> getUserByEmail(final String email) {
		return new Specification<User>() {

			@Override
			public Predicate toPredicate(Root<User> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
				return criteriaBuilder.equal(root.get("email"), email);
			}
		};
	}

	public static Specification<User> getUserByEmailOrUserName(final String email, final String userName) {
		return new Specification<User>() {

			@Override
			public Predicate toPredicate(Root<User> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
				Predicate userEmailEqual = criteriaBuilder.equal(root.get("email"), email);
				Predicate userNameEqual = criteriaBuilder.equal(root.get("userName"), userName);
				return criteriaBuilder.or(userEmailEqual, userNameEqual);
			}
		};
	}

	public static Specification<User> getUserByUserName(final String userName) {
		return new Specification<User>() {

			@Override
			public Predicate toPredicate(Root<User> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
				return criteriaBuilder.equal(root.get("userName"), userName);
			}
		};
	}

	public static Specification<User> getUserByActivateCode(final String activationCode) {
		return new Specification<User>() {

			@Override
			public Predicate toPredicate(Root<User> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
				return criteriaBuilder.equal(root.get("activationCode"), activationCode);
			}
		};
	}

}
