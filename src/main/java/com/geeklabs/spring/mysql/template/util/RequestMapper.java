package com.geeklabs.spring.mysql.template.util;


public final class RequestMapper {

	private RequestMapper() {}

	
	public final static String USER_LOGIN_SUCCESS = "/login/success";
	public final static String USER_LOGIN_ERROR = "/login/error";
	
	public final static String HOME = "/";
	
	public final static String ABOUT_US = "/aboutus";
	public final static String CONTACT_US = "/contactus";
	
	public final static String EBAY = "/ebay";
	public final static String PUSH_TO_MARKET = "/pushToMarket/{productId}";
	public final static String PUSH_TO_MARKET_LIST_OF_PRODUCTS = "/pushToMarket";
	
	public static final String IS_ACTIVATED = "/activate";
	
	/** Inventory */
	public final static String INVENTORY = "/retailer";
	public final static String MY_INVENTORY = "/myinventory";
	public final static String SAVE_INVENTORY = "/myinventory/add";			
	public static final String INVENTORY_WITH_ID = "/myinventory/{inventoryId}";
	
	public static final String MY_INVENTORY_LIST  = "/inventory/list/";
	public static final String REMOVE_PRODUCTS_INVENTORY_LIST  = "/inventory/removeProducts/{inventoryId}";
	public static final String DELETE_INVENTORY_PRODUCT = "";
	
	/** Order */
	public static final String GET_ORDERS = "/getOrders";
	public final static String ORDER = "/order";
	public static final String GET_ORDER_BY_SUPPLIER = "/bysupplier/{orderId}";
	
	/** PAYMENT **/
	public static final String PAY = "/retailer/pay";
	public static final String PAYMENT_METHOD = "creditcard/{orderId}";
	
	public static final String CREDITCARD = "/creditcard/{orderId}";
	public static final String PAYPAL = "/paypal/{productOrderId}";
	public static final String EXECUTE = "/execute";
	public static final String CANCEL = "/cancel";
	
	/** PRODUCT SUPPPLIER **/
	public static final String PRODUCT = "/supplier/product";
	public static final String ADD_PRODUCT = "/add";
	
	/** PRODUCT **/
	public static final String PRODUCT_FEED = "/products";
	public static final String PRODUCT_DETAIL = "/detail/{productId}";
	public static final String GET_PRODUCT_FEED = "/productfeed";
	public static final String PRODUCT_FILTER = "/productfeed/{filter}";
	public static final String EDIT_PRODUCT = "/edit/{productId}";
	public static final String UPDATE_PRODUCT = "/update/";
	public static final String DELETE_PRODUCT = "/delete/{productId}";
	
	public static final String RETAILER = "/retailer";
	public static final String RETAILER_REGISTER = "/register";
	public static final String SUPPLIER = "/supplier";
	public static final String SUPPLIER_REGISTER = "/register";
	
	/** SUPPLIER DASHBOARD */
	public static final String SUPPLIER_DASHBOARD = "/dashboard";
	public static final String GET_SUPPLIER_PRODUCT = "/dashboard/get/{pageNo}";
	
	/** CART */
	public static final String CART = "/retailer/cart";
	public static final String GET_CART = "";
	public static final String GET_CART_BY_SUPPLIER = "/bysupplier";
	public static final String UPDATE_ORDERITEM_QUANTITY = "/updateOrderItemQuantity";
	
	public static final String SAVE_ORDERITEMS = "/add/{retailerId}";
	public static final String DELETE_ORDER_ITEM = "/deleteOrderItem/{orderItemId}";
	
	/** SHIPPING  */ 
	public static final String SHIPPING_ADDRESS = "/retailer/shipping";
	public static final String GET_SHIPPING_ADDRESS = "/{orderId}";
	public static final String ADD_SHIPPING_ADDRESS = "/{orderId}/add";
	public static final String GET_SHIPPING_ADDRESS_BY_RETAILER = "/getAddressByRetailer/{retailerId}";
	
	public static final String UPDATE = "/update";
	public static final String UPDATE_CART_ITEM = "/updateOrderItem";
	public static final String ADD_TO_CART = "/addItems" ;
	public static final String ADDDELETE = "/adddelete" ;
	public static final String DELETE = "/delete/{id}" ;
	
	/** Product Order */
	public static final String PRODUCT_ORDER = "/retailer/order";
	public static final String CREATE = "/create/" ;
	public static final String GET_ALL_ORDERS_BY_RETAILER = "/list";
	public static final String GET_ORDER_BY_STATUS = "/status/{orderId}";	
	public static final String PROCEED_CHECKOUT = "/proceedcheckout/{retailerId}" ;
	public static final String GET_ORDER_BY_ID = "/{orderId}";
	public static final String GET_UN_PAID_ORDERS = "/unPaidOrders";
	
	/** Card **/
	public static final String CARD = "/card" ;
	public static final String ADD_CARD = "/add" ;
	
	/** Category **/
	public static final String CATEGORY = "/admin/category" ;
	public static final String ADD_CATEGORY = "/add";
	public static final String LIST_CATEGORY = "/list";
	public static final String GET_CATEGORY_LIST = "/list/get";
	public static final String UPDATE_CATEGORY = "update";
	public static final String DELETE_CATEGORY = "/delete/{categoryId}";
	public static final String GET_CATEGORY = "/get/{categoryId}";
	public static final String SAVE_CATEGORY_LIST = "/ebayCategories";
	
	/**
	 * Search
	 */
	public static final String SEARCH = "/search";
	
}