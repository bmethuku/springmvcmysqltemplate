package com.geeklabs.spring.mysql.template.config;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

@Configuration
@PropertySource("classpath:application.properties" )
public class CoreConfig {
	
	@Autowired
	Environment env;
	
	@Bean
	public JavaMailSender mailSender() {
		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
		mailSender.setUsername(env.getProperty("mail.smtp.user"));
		mailSender.setPassword(env.getProperty("mail.smtp.password"));
		mailSender.setHost(env.getProperty("mail.smtp.host"));
		mailSender.setPort(Integer.parseInt(env.getProperty("mail.smtp.port")));
		mailSender.setJavaMailProperties(getJavaMailProperties());
		
		return mailSender;
		
	}

	private Properties getJavaMailProperties() {
		Properties javaMailProperties = new Properties();
		javaMailProperties.put("mail.smtp.auth", env.getProperty("mail.smtp.auth"));
		javaMailProperties.put("mail.smtp.starttls.enable", env.getProperty("mail.smtp.starttls.enable"));

		return javaMailProperties;
	}
}
