package com.geeklabs.spring.mysql.template.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.geeklabs.spring.mysql.template.domain.User;
import com.geeklabs.spring.mysql.template.domain.repository.custom.UserRepositoryCustom;

public interface UserRepository extends UserRepositoryCustom, JpaRepository<User, Long>, JpaSpecificationExecutor<User> {
}
