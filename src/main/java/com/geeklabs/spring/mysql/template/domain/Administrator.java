package com.geeklabs.spring.mysql.template.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import com.google.common.base.Objects;

@Entity
public class Administrator {

	@Id
	@GeneratedValue
	private Long id;
	@NotNull
	private String email;
	@NotNull
	private String adminName;
	
	
	@Version
	private int optLockVersion;
	
	public int getOptLockVersion() {
		return optLockVersion;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAdminName() {
		return adminName;
	}
	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}
	public Long getId() {
		return id;
	}
	
	@Override
	public int hashCode() {
		return Objects.hashCode(this.id);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Administrator administrator = (Administrator) obj;
		return Objects.equal(this.id, administrator.getId());
	}
}
