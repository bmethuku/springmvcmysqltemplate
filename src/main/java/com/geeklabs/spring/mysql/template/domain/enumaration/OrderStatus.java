package com.geeklabs.spring.mysql.template.domain.enumaration;

public enum OrderStatus {

	NEW, HOLD, PAYMENTSUCCESS, DELIVERED, CLOSED;
}
