package com.geeklabs.spring.mysql.template.service;

import com.geeklabs.spring.mysql.template.domain.UserRole;
import com.geeklabs.spring.mysql.template.enums.UserRoles;

public interface UserRoleService {

	UserRole getUserRoleByRoleName(UserRoles retailer);

	
}
