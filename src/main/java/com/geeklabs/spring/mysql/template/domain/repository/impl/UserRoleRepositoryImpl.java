package com.geeklabs.spring.mysql.template.domain.repository.impl;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.geeklabs.spring.mysql.template.domain.UserRole;
import com.geeklabs.spring.mysql.template.domain.repository.custom.UserRoleRepositoryCustom;
import com.geeklabs.spring.mysql.template.enums.UserRoles;

public class UserRoleRepositoryImpl implements UserRoleRepositoryCustom{

	@Override
	public Specification<UserRole> getUserRoleByRoleName(final UserRoles roleName) {
		return new Specification<UserRole>() {
			
			@Override
			public Predicate toPredicate(Root<UserRole> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
				return criteriaBuilder.equal(root.get("userRole"), roleName);
			}
		};
	}
}
