package com.geeklabs.spring.mysql.template.service;

import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.geeklabs.spring.mysql.template.domain.User;
import com.geeklabs.spring.mysql.template.domain.repository.UserRepository;
import com.geeklabs.spring.mysql.template.util.ResponseStatus;

public interface UserService extends UserDetailsService {

	ResponseStatus isAuthenticatedUser(User user);
	
	User getUserByEmail(String email);
	
	void setUserRepository(UserRepository userRepository);
	
	User getUserByUsername(String username);
	
	User getUserByEamailOrUserName(String userName, String email);

	ResponseStatus isUserActivated(String activate);

	void setPasswordEncoder(BCryptPasswordEncoder passwordEncoder);

}
