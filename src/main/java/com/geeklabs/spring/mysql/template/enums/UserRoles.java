package com.geeklabs.spring.mysql.template.enums;

public enum UserRoles {

	RETAILER,
	SUPPLIER,
	ADMIN
}
