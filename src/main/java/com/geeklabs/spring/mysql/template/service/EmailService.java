package com.geeklabs.spring.mysql.template.service;

import javax.servlet.http.HttpServletRequest;

import com.geeklabs.spring.mysql.template.dto.UserDto;

public interface EmailService {

	void  sendWelcomeMail(UserDto userDto, HttpServletRequest request);
	
	void  sendMail(String toMail, String subject, String message);

	void sendOrderSuccess(UserDto userDto);

	void sendTransactionSuccess(UserDto userDto);

	void sendOrderFailureMessage(UserDto userDto);

	
}
