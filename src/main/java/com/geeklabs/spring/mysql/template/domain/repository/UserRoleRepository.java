package com.geeklabs.spring.mysql.template.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.geeklabs.spring.mysql.template.domain.UserRole;
import com.geeklabs.spring.mysql.template.domain.repository.custom.UserRoleRepositoryCustom;

public interface UserRoleRepository extends UserRoleRepositoryCustom, JpaRepository<UserRole, Long>, JpaSpecificationExecutor<UserRole> {
}
