package com.geeklabs.spring.mysql.template.domain.repository.impl;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Repository;

import com.geeklabs.spring.mysql.template.domain.User;
import com.geeklabs.spring.mysql.template.domain.repository.custom.UserRepositoryCustom;
import com.geeklabs.spring.mysql.template.domain.repository.spec.UserSpecification;

@Repository
public class UserRepositoryImpl implements UserRepositoryCustom {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public User isValidUser(String email, String password) {

		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<User> criteriaQuery = criteriaBuilder.createQuery(User.class);

		Root<User> from = criteriaQuery.from(User.class);
		criteriaQuery.select(from);

		Predicate userEqual = criteriaBuilder.equal(from.get("email"), email);
		Predicate passwordEqual = criteriaBuilder.equal(from.get("password"), password);

		criteriaQuery.where(userEqual, passwordEqual);
		User user = null;
		try {
			user = entityManager.createQuery(criteriaQuery).getSingleResult();
			return user;
		} catch (NoResultException noResultException) {
			return null;
		}
	}

	@Override
	public User isUserExit(String email, String userName) {
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<User> criteriaQuery = criteriaBuilder.createQuery(User.class);

		Root<User> from = criteriaQuery.from(User.class);
		criteriaQuery.select(from);

		Predicate userEmailEqual = criteriaBuilder.equal(from.get("email"), email);
		Predicate userNameEqual = criteriaBuilder.equal(from.get("userName"), userName);
		Predicate predicate = criteriaBuilder.or(userEmailEqual, userNameEqual);
		criteriaQuery.where(predicate);
		User user = null;
		try {
			user = entityManager.createQuery(criteriaQuery).getSingleResult();
			return user;
		} catch (NoResultException noResultException) {
			return null;
		}
	}
	
	@Override
	public Specification<User> getUserByActivateCode(String activationCode) {
		return UserSpecification.getUserByActivateCode(activationCode);
	}
}
