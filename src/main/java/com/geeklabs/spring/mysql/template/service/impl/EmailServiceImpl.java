package com.geeklabs.spring.mysql.template.service.impl;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.geeklabs.spring.mysql.template.dto.UserDto;
import com.geeklabs.spring.mysql.template.service.EmailService;
import com.geeklabs.spring.mysql.template.util.WebHelper;

@Service
public class EmailServiceImpl implements EmailService {
	
	@Autowired
	private Environment env;
	
	@Autowired
	private JavaMailSender mailSender;
	
	@Autowired 
	private TemplateEngine templateEngine;
	
	@Override
	public void sendWelcomeMail(UserDto userDto, HttpServletRequest request) {
		
		final Context ctx = new Context(); //TODO later define the locale for the context when we do internalization			
		String activationLink = WebHelper.getContextPath(request) + "/user/activate/?activate=" + userDto.getActivationCode();			
		ctx.setVariable("activationLink", activationLink);		
		ctx.setVariable("user", userDto);			
		final String htmlContent = this.templateEngine.process("welcomeMail", ctx);		
		send(userDto.getEmail(), "Welcome to the flipgoods online store", htmlContent);
	}
	
	@Override
	public void sendMail(String toMail, String subject, String body) {
		send(toMail, subject, body);
	}

	private void send(String toMail, String subject, String body) {
		final String username = env.getProperty("mail.smtp.user");
		
		final MimeMessage mimeMessage = this.mailSender.createMimeMessage();
		final MimeMessageHelper message = new MimeMessageHelper(mimeMessage);

		try {
			
			message.setFrom(username);
			message.setTo(toMail);
			message.setSubject(subject);
			message.setText(body, true);
			
		} catch (MessagingException e) {
			throw new RuntimeException(e); 
		}
		
		this.mailSender.send(mimeMessage);
	}

	@Override
	public void sendOrderSuccess(UserDto userDto) {
		final Context ctx = new Context(); //TODO later define the locale for the context when we do internalization
		ctx.setVariable("user", userDto);
		final String htmlContent = this.templateEngine.process("orderSuccessMail", ctx);
		
		send(userDto.getEmail(), "Welcome to the flipgoods online store", htmlContent);
	}

	@Override
	public void sendTransactionSuccess(UserDto userDto) {
		final Context ctx = new Context(); //TODO later define the locale for the context when we do internalization
		ctx.setVariable("user", userDto);
		final String htmlContent = this.templateEngine.process("transactionSuccessMail", ctx);
		
		send(userDto.getEmail(), "Welcome to the flipgoods online store", htmlContent);
	}
	
	@Override
	public void sendOrderFailureMessage(UserDto userDto) {
		
		final Context ctx = new Context(); //TODO later define the locale for the context when we do internalization
		ctx.setVariable("user", userDto);
		final String htmlContent = this.templateEngine.process("orderFailureMail", ctx);
		
		send(userDto.getEmail(), "Welcome to the flipgoods online store", htmlContent);
		
	}
}
