package com.geeklabs.spring.mysql.template.domain.repository.custom;

import org.springframework.data.jpa.domain.Specification;

import com.geeklabs.spring.mysql.template.domain.User;


public interface UserRepositoryCustom {

	User isValidUser(String email, String password);
	
	User isUserExit(String email, String userName);
	
	Specification<User> getUserByActivateCode(String activate);
}
