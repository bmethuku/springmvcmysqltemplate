package com.geeklabs.spring.mysql.template.domain.repository.custom;

import org.springframework.data.jpa.domain.Specification;

import com.geeklabs.spring.mysql.template.domain.UserRole;
import com.geeklabs.spring.mysql.template.enums.UserRoles;

public interface UserRoleRepositoryCustom {

	Specification<UserRole> getUserRoleByRoleName(UserRoles roleName);
}
