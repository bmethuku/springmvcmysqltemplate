package com.geeklabs.spring.mysql.template.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geeklabs.spring.mysql.template.domain.CustomUserDetails;
import com.geeklabs.spring.mysql.template.domain.User;
import com.geeklabs.spring.mysql.template.domain.enumaration.UserStatus;
import com.geeklabs.spring.mysql.template.domain.repository.UserRepository;
import com.geeklabs.spring.mysql.template.domain.repository.spec.UserSpecification;
import com.geeklabs.spring.mysql.template.enums.UserRoles;
import com.geeklabs.spring.mysql.template.service.UserService;
import com.geeklabs.spring.mysql.template.util.ResponseStatus;

@Service("userService")
@Qualifier("userService")
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	@Override
	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}
	
	@Override
	public void setPasswordEncoder(BCryptPasswordEncoder passwordEncoder) {
		this.passwordEncoder = passwordEncoder;
	}
	
	@Override
	@Transactional(readOnly = true)
	public ResponseStatus isAuthenticatedUser(User user) {
		User userByEmail = getUserByEmail(user.getEmail());

		ResponseStatus responseStatus = new ResponseStatus();
		if (userByEmail != null) {
			if (UserRoles.RETAILER == userByEmail.getUserRole()) {
				responseStatus.setStatus("RETAILER");
			}
			responseStatus.setStatus("SUPPLIER");
			return responseStatus;
		}
		return responseStatus;
	}

	@Override
	@Transactional(readOnly = true)
	public User getUserByEmail(String email) {
		Specification<User> userByEmail = UserSpecification.getUserByEmail(email);
		User user = userRepository.findOne(userByEmail);
		return user;
	}

	@Override
	public UserDetails loadUserByUsername(String userNameOrEmail)
			throws UsernameNotFoundException {
		User userByEamailOrUserName = getUserByEamailOrUserName(userNameOrEmail, userNameOrEmail);
		
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		if (UserRoles.RETAILER == userByEamailOrUserName.getUserRole()) {
			authorities.add(new SimpleGrantedAuthority("ROLE_" + UserRoles.RETAILER.toString()));
		} else if (UserRoles.SUPPLIER == userByEamailOrUserName.getUserRole()) {
			authorities.add(new SimpleGrantedAuthority("ROLE_" + UserRoles.SUPPLIER.toString()));
		} else if (UserRoles.ADMIN == userByEamailOrUserName.getUserRole()) {
			authorities.add(new SimpleGrantedAuthority("ROLE_" + UserRoles.ADMIN.toString()));
		}		
		
		String password = userByEamailOrUserName.getPassword();
		String userStatus = userByEamailOrUserName.getUserStatus().toString();
		if (userStatus.equalsIgnoreCase("ACTIVE")) {
			UserDetails user = new CustomUserDetails(userByEamailOrUserName.getUserName(), password, authorities, userByEamailOrUserName.getId());
			return user;	
		}
		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public User getUserByUsername(String username) {		
		Specification<User> userByUserName = UserSpecification.getUserByUserName(username);
		User user = userRepository.findOne(userByUserName);
		return user;
	}

	@Override
	@Transactional(readOnly = true)
	public User getUserByEamailOrUserName(String userName, String email) {
		Specification<User> userByEmailOrUserName = UserSpecification.getUserByEmailOrUserName(email, userName);
		return userRepository.findOne(userByEmailOrUserName);
	}
	
	@Override
	@Transactional
	public ResponseStatus isUserActivated(String activationCode) {
		
		ResponseStatus responseStatus = new ResponseStatus();
		Specification<User> userSpecification = userRepository.getUserByActivateCode(activationCode);
		User user = userRepository.findOne(userSpecification);
		if (user != null) {
			user.setUserStatus(UserStatus.ACTIVE);
			userRepository.save(user);
			responseStatus.setStatus("success");
			return responseStatus;
		} 
		return responseStatus;
	}
}
