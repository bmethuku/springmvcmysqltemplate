package com.geeklabs.spring.mysql.template.domain.enumaration;

public enum UserStatus {
	NEW, ACTIVE, BANNED, BLOCKED, INACTIVE;
}
